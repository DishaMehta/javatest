package com.hibernate.ServiceClasses;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import com.hibernate.EntityClasses.customer;
import com.hibernate.EntityClasses.customerLedger;
import com.hibernate.EntityClasses.schedule;
import com.hibernate.UtilityClasses.hibernateUtil;




public class creditCardService {

	Transaction transaction = null;
	public void addLedgerWithPurchase(String type,int amount,String status,int custId) {
		customerLedger ld = null;
		int amt = -(amount);
		int balance=0;
		customer c = getCustomer(custId);
		customerLedger le = getLedgerBalance(c.getcustomerId());
		if(le==null)
			balance = amt;
		else {
			balance = le.getBalance() + amt;
		}
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
						
			ld = new customerLedger(type,amt,balance,status);
			ld.setCustomer(c);
			session.save(ld);
			
			transaction.commit();
			addSchedule(ld,amount/4);
		}catch(Exception ex) {
			System.out.println(ex);
				if(transaction != null) {
					transaction.rollback();
				}
		}
		
		
	}
	
	public void addSchedule(customerLedger ledger,int amount) {
		int cnt=1;
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			
		while(cnt!=5) {
			schedule schedule = new schedule(cnt,"unpaid",amount);
			schedule.setCustomerLedger(ledger);
			session.save(schedule);
			cnt++;
		}
			transaction.commit();
		}catch(Exception ex) {
			System.out.println(ex);
				if(transaction != null) {
					transaction.rollback();
				}
		}
	}
	
	public customer getCustomer(int id) {
		customer c =null;
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(customer.class);
			cr.add(Restrictions.eq("customerId",id));
			Object result = cr.uniqueResult();
			c = (customer)result;
			transaction.commit();
		}catch(Exception ex) {
				ex.printStackTrace();
		}
		return c;
	}
	
	
	public customerLedger getLedgerBalance(int customerid) {
		customerLedger cl =null;
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			String str = "from customerLedger where customer_customerid = :customerid order by id desc";
			Query query = session.createQuery(str);
			query.setParameter("customerid", customerid);
			query.setMaxResults(1);
			List rs = query.list();
			if(rs.size()==0)
				return cl;
			cl = (customerLedger)rs.get(0);
			transaction.commit();
		}catch(Exception ex) {
				ex.printStackTrace();
		}
		return cl;
	}
	

	public void updateSchedulePayment(int customerId) {
		int amount=0;
		schedule sc = null;
		int ledgerId = getLedgerDetails(customerId);
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			String str = "from schedule where customerledger_id = :ledgerId and status = :status order by scheduleid asc";
			Query query = session.createQuery(str);
			query.setParameter("ledgerId", ledgerId);
			query.setParameter("status", "unpaid");
			query.setMaxResults(1);
			List rs = query.list();
			sc = (schedule)rs.get(0);
			amount =sc.getAmount();
			sc.setStatus("paid");
			
			session.save(sc);
			
			transaction.commit();
			
			addToLedger(customerId,sc.getAmount());
			
		}catch(Exception ex) {
				ex.printStackTrace();
				System.out.println(ex);
		}
	}
	
	public int getLedgerDetails(int customerId) {
		customerLedger cusLed = new customerLedger();
		int LedgerId = 0;
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			String str = "from customerLedger where customer_customerid = :customerId and status =:status order by id asc";
			Query query = session.createQuery(str);
			query.setParameter("customerId", customerId);
			query.setParameter("status", "Open");
			query.setMaxResults(1);
			List rs = query.list();
			cusLed = (customerLedger)rs.get(0);
			LedgerId = cusLed.getId();
			transaction.commit();
			
		}catch(Exception ex) {
				ex.printStackTrace();
				System.out.println(ex);
		}
		return LedgerId;
	}
	
	public void addToLedger(int customerId,int amount) {
		
		customerLedger cd = new customerLedger();
		cd.setAmount(amount);
		cd.setType("Payment");
		cd.setStatus("Complete");
		customer c = getCustomer(customerId);
		cd.setCustomer(c);
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			session.save(cd);
			transaction.commit();
			
			int balance = getCustomerLedgerLatestBalance(customerId);
			int updatedBalance =UpdateLedgerBalance(cd.getId(),balance,amount);
			if(updatedBalance==0)
				updateLedgerStatus(customerId);
			
		}catch(Exception ex) {
			System.out.println(ex);
				if(transaction != null) {
					transaction.rollback();
				}
		}
		

	}
	
	public int getCustomerLedgerLatestBalance(int customerId) {
		customerLedger cusLed = new customerLedger();
		int balance = 0;
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			String str = "from customerLedger where customer_customerid = :customerId order by id desc";
			Query query = session.createQuery(str);
			query.setParameter("customerId", customerId);
			query.setMaxResults(2);
			List rs = query.list();
			cusLed = (customerLedger)rs.get(1);
			balance = cusLed.getBalance();
			System.out.println("balance---"+balance);
			transaction.commit();
			
		}catch(Exception ex) {
				ex.printStackTrace();
				System.out.println(ex);
		}
		return balance;
		
	}
	
	public int UpdateLedgerBalance(int ledgerId,int balance,int amount) {
		int res=0;
		int updatedBalance =0;
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			customerLedger customerLedger = new customerLedger();
			String str = "Update customerLedger set balance = :balance where id = :ledgerId";
			Query query = session.createQuery(str);
			query.setParameter("ledgerId", ledgerId);
			updatedBalance = balance + amount;
			query.setParameter("balance", updatedBalance);
			res = query.executeUpdate();
			transaction.commit();
			
		}catch(Exception ex) {
				ex.printStackTrace();
				System.out.println(ex);
		}
		
		return updatedBalance;
	}
	
	public void updateLedgerStatus(int customerId) {
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			customerLedger customerLedger = new customerLedger();
			String str = "from customerLedger where status = :status and customer_customerid = :customerId order by id asc";
			Query query = session.createQuery(str);
			query.setParameter("status", "Open");
			query.setParameter("customerId", customerId);
			query.setMaxResults(1);
			List rs = query.list();
			customerLedger ledger = (customerLedger)rs.get(0);
			ledger.setStatus("Complete");
			
			session.save(ledger);
			transaction.commit();
			
		}catch(Exception ex) {
				ex.printStackTrace();
				System.out.println(ex);
		}
	}
}
