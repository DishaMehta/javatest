package com.hibernate.App;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.EntityClasses.customer;
import com.hibernate.EntityClasses.customerLedger;
import com.hibernate.ServiceClasses.creditCardService;
import com.hibernate.UtilityClasses.hibernateUtil;


public class App {

	public static void main(String[] args) {
		Transaction transaction = null;
		customer cus = new customer("Disha","Mehta","21-08-1997");
		customer cus2 = new customer("Yash","Shah","18-02-1998");
		
		try(Session session = hibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			
			session.save(cus);
			session.save(cus2);
			
			transaction.commit();
			
					}catch(Exception ex) {
			System.out.println(ex);
		}
		creditCardService cr = new creditCardService();
		cr.addLedgerWithPurchase("Purchase",40,"Open", cus.getcustomerId());
		cr.updateSchedulePayment(cus.getcustomerId());
		cr.updateSchedulePayment(cus.getcustomerId());
		cr.addLedgerWithPurchase("Purchase", 40, "Open", cus2.getcustomerId());
		cr.updateSchedulePayment(cus2.getcustomerId());
		cr.updateSchedulePayment(cus.getcustomerId());
//		cr.addLedgerWithPurchase("Purchase", 80, "Open", 2);
//		cr.updateSchedulePayment(1,1);
//		cr.addLedgerWithPurchase("Purchase", 40, "Open", 1);
//		cr.updateSchedulePayment(1,1);
		
		
		
		
	}

}
