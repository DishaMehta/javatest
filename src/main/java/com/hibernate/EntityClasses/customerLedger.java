package com.hibernate.EntityClasses;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class customerLedger {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String type;
	private int amount;
	private int balance;
	private String status; 
	@ManyToOne
	private customer customer;
	
	@OneToMany(mappedBy="customerLedger")
	private Collection<schedule>schedules = new ArrayList<schedule>();

	
	public customerLedger() {
		
	}

	public customerLedger(String type, int amount, int balance, String status) {
		super();
		this.type = type;
		this.amount = amount;
		this.balance = balance;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public customer getCustomer() {
		return customer;
	}

	public void setCustomer(customer customer) {
		this.customer = customer;
	}

	public Collection<schedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(Collection<schedule> schedules) {
		this.schedules = schedules;
	}
	
	
	
	
}
